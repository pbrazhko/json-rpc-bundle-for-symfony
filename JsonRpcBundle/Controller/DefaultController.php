<?php

namespace Brazhko\JsonRpcBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Brazhko\JsonRpcBundle\Classes\JsonRpc;

class DefaultController extends Controller {

    public function indexAction() {
        $request = $this->getRequest();

        if ($request->getMethod() !== 'POST' || $request->getContentType() !== 'json') {
            throw $this->createNotFoundException();
        }

        $jsonRpc = new JsonRpc($this->container->get('kernel'));

        return new Response($jsonRpc->getResponse());
    }

}