<?php

namespace Brazhko\JsonRpcBundle\Interfaces;

/**
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 */
interface JsonRpcInterface {
    public function setAllowedMethods();
}