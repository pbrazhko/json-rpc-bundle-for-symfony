<?php

namespace Brazhko\JsonRpcBundle\Classes;

use Exception;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Symfony\Component\Serializer\Encoder\JsonEncode;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

/**
 * Description of JsonRpc
 *
 * @author Brazhko Pavel <cofs2006@gmail.com>
 * @version "1.2"
 */
class JsonRpc {

    const JSON_INVALID_REQUEST_ERROR = -32600;
    const JSON_METHOD_NOT_FOUND_ERROR = -32601;
    const JSON_INVALID_PARAMS_ERROR = -32602;
    const JSON_INTERNAL_ERROR = -32603;
    const JSON_PARSE_ERROR = -32700;
    const JSON_VERSION_NOT_SUPPORTED_ERROR = -320001;
    const JSON_BUNDLE_NOT_DEFINED_ERROR = -320002;
    const JSON_CONTROLLER_NOT_DEFINED_ERROR = -320003;
    const JSON_ACTION_NOT_DEFINED_ERROR = -320004;
    const JSON_EXECUTING_ERROR = -320005;

    private $errors = array(
        self::JSON_INVALID_REQUEST_ERROR => 'Invalid Request',
        self::JSON_METHOD_NOT_FOUND_ERROR => 'Method not found',
        self::JSON_INVALID_PARAMS_ERROR => 'Invalid params',
        self::JSON_INTERNAL_ERROR => 'Internal error',
        self::JSON_PARSE_ERROR => 'Parse error',
        self::JSON_VERSION_NOT_SUPPORTED_ERROR => 'Not supported version JSON-RPC',
        self::JSON_BUNDLE_NOT_DEFINED_ERROR => 'Param "bundle" is not defined',
        self::JSON_CONTROLLER_NOT_DEFINED_ERROR => 'Param "controller" is not defined',
        self::JSON_ACTION_NOT_DEFINED_ERROR => 'Param "action" is not defined',
    );

    /**
     * @var array
     */
    private $request = null;

    /**
     * @var array 
     */
    private $response = null;

    /**
     * @var array
     */
    private $bundles = array();
    private $kernel;

    /**
     * @var JsonEncode 
     */
    private $encoder;

    /**
     * @var JsonDecode 
     */
    private $decoder;

    public function __construct(Kernel $kernel) {
        $this->kernel = $kernel;
        $this->bundles = $this->kernel->getBundles();
        $this->encoder = new JsonEncode();
        $this->decoder = new JsonDecode();
    }

    /**
     * Sending error
     * 
     * @param integer $id
     * @param integer $code
     * @param string $message
     * @return string
     */
    public function sendError($id, $code, $message = '') {
        if (isset($this->errors[$code])) {
            $message = $this->errors[$code];
        }

        return $this->encoder->encode(array(
                    'id' => $id,
                    'error' => array('code' => $code, 'message' => $message)
                        ), JsonEncoder::FORMAT);
    }

    /**
     * @return string
     */
    public function getResponse() {
        if ($this->parseRequest()) {
            try {
                $this->response = $this->execute();
            } catch (Exception $e) {
                $this->response = $this->sendError(0, -320005, $e->getMessage());
            }
        }

        return $this->response;
    }

    /**
     * Geting request and validation
     * 
     * @return boolean
     */
    public function parseRequest() {
        $json = file_get_contents('php://input');

        $this->request = $this->decoder->decode($json, JsonEncoder::FORMAT);

        if (!$this->validationRequest()) {
            return false;
        }

        return true;
    }

    /**
     * Executing request
     * 
     * @return string
     * @throws InvalidArgumentException
     * @throws Exception
     */
    private function execute() {
        $bundle = $this->kernel->getBundle($this->request->bundle);
        if (null === $bundle) {
            throw new InvalidArgumentException('Bundle not found!');
        }

        $bundleControllers = $this->getControllers($bundle);

        if (!isset($bundleControllers[$this->request->controller . 'Controller'])) {
            throw new InvalidArgumentException('Controller not found!');
        }

        try {
            return $this->executeController(
                            $bundle->getName(), $this->request->controller . 'Controller', $this->request->action, $this->request->params
            );
        } catch (Exception $e) {
            return $this->sendError('0', self::JSON_EXECUTING_ERROR, $e->getMessage());
        }
    }

    /**
     * 
     * @param string $bundle
     * @param string $controller
     * @param string $action
     * @param array $params
     * @return mixed
     */
    private function executeController($bundle, $controller, $action, $params) {

        $bundle = $this->kernel->getBundle($bundle);

        $class = $this->getController($bundle, $controller);

        return $this->executeAction($class, strtolower($action) . 'Action', $params);
    }

    /**
     * 
     * @param object $controller
     * @param string $action
     * @param array $params
     * @return mixed
     * @throws InvalidArgumentException
     */
    private function executeAction($controller, $action, $params) {
        $reflection = new ReflectionMethod($controller, $action);

        if (is_array($params)){
            
        }
        
        $methodArgs = array();
        foreach ($reflection->getParameters() as $param) {
            $paramName = $param->name;
            if (!isset($params->$paramName)) {
                throw new InvalidArgumentException(sprintf('Parameter "%s" is not defined!', $paramName));
            }

            array_push($methodArgs, $params->$paramName);
        }

        return $reflection->invokeArgs($controller, $methodArgs);
    }

    /**
     * 
     * @param object $bundle
     * @param string $controller
     * @return object
     * @throws InvalidArgumentException
     */
    private function getController($bundle, $controller) {
        $reflection = new ReflectionClass($bundle->getNamespace() . '\\Controller\\' . $controller);

        $constructorArgs = array();
        $constructor = $reflection->getConstructor();
        foreach ($constructor->getParameters() as $param) {
            if (!$this->kernel->getContainer()->has($param->name)) {
                throw new InvalidArgumentException(printf('Parameter for constructor "%s" is not defined in container!', $param->name));
            }

            array_push($constructorArgs, $this->kernel->getContainer()->get($param->name));
        }

        $object = $reflection->newInstanceArgs($constructorArgs);

        $object->setContainer($this->kernel->getContainer());

        return $object;
    }

    /**
     * Getting bundle controlles
     * 
     * @param object $bundleObject
     * @return array
     */
    private function getControllers($bundleObject) {
        $ret = array();

        $controllerDir = $bundleObject->getPath() . '/Controller';
        if (is_dir($controllerDir)) {
            $finder = new Finder;
            $namespace = $bundleObject->getNamespace() . '\\Controller';
            $finder->files()->name('*Controller.php')->in($controllerDir);
            foreach ($finder as $file) {
                $ns = $namespace;
                if ($relativePath = $file->getRelativePath()) {
                    $ns .= '\\' . strtr($relativePath, '/', '\\');
                }
                $className = $file->getBasename('.php');
                $r = new ReflectionClass($ns . '\\' . $file->getBasename('.php'));
                if ($r->isInstantiable() && $className != 'Controller') {
                    $ret[$className] = $ns . '\\' . $file->getBasename('.php');
                }
            }
        }

        return $ret;
    }

    /**
     * Validation request
     * 
     * @return boolean
     */
    private function validationRequest() {
        if (!isset($this->request->jsonrpc) || $this->request->jsonrpc !== '2.0') {
            $this->response = $this->sendError(0, self::JSON_VERSION_NOT_SUPPORTED_ERROR);
            return false;
        }

        if (!isset($this->request->bundle) || is_null($this->request->bundle)) {
            $this->response = $this->sendError(0, self::JSON_BUNDLE_NOT_DEFINED_ERROR);
            return false;
        }

        if (!isset($this->request->controller) || is_null($this->request->controller)) {
            $this->response = $this->sendError(0, self::JSON_CONTROLLER_NOT_DEFINED_ERROR);
            return false;
        }

        if (!isset($this->request->action) || is_null($this->request->action)) {
            $this->response = $this->sendError(0, self::JSON_ACTION_NOT_DEFINED_ERROR);
            return false;
        }

        return true;
    }

}